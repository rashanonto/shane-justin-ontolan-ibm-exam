package ontolan.justin.shane.openweather.helper;

import android.content.Context;
import android.content.SharedPreferences;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SessionHelper {

    private final String PREF_DATE_TODAY = "dateToday";
    private final String PREF_LOCATION_ID = "id";
    private final String PREF_NAME = "openweather";
    private final String PREF_TAG = "0";
    int PRIVATE_MODE = 0;

    Context _context;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    public SessionHelper(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void setDateToday() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());

        pref = _context.getSharedPreferences(PREF_DATE_TODAY, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(PREF_DATE_TODAY, "Last Update : " + currentDateandTime);
        editor.apply();
    }

    public void setLocationId(String id) {
        pref = _context.getSharedPreferences(PREF_LOCATION_ID, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(PREF_LOCATION_ID, id);
        editor.apply();
    }

    public void setTag(String id) {
        pref = _context.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(PREF_TAG, id);
        editor.apply();
    }

    public String getDateTime() {
        return _context.getSharedPreferences(PREF_DATE_TODAY, Context.MODE_PRIVATE).getString(PREF_DATE_TODAY, "");
    }
    public String getLocationId() {
        return _context.getSharedPreferences(PREF_LOCATION_ID, Context.MODE_PRIVATE).getString(PREF_LOCATION_ID, "");
    }
    public String getTag() {
        return _context.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE).getString(PREF_TAG, "");
    }

}
