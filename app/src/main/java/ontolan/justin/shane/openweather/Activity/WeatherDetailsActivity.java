package ontolan.justin.shane.openweather.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.koushikdutta.ion.Ion;
import java.util.List;
import ontolan.justin.shane.openweather.R;
import ontolan.justin.shane.openweather.helper.ConnectivityHelper;
import ontolan.justin.shane.openweather.helper.DbHelper;
import ontolan.justin.shane.openweather.helper.SessionHelper;
import ontolan.justin.shane.openweather.object.WeatherObject;

/**
 * Created by rashanonto on 2018-06-15.
 */

public class WeatherDetailsActivity extends AppCompatActivity {

    private DbHelper dbHelper;
    private SessionHelper sessionHelper;
    private WeatherObject weatherObject;
    private List<WeatherObject> listData;
    private ConnectivityHelper connectivityHelper;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);

        dbHelper = new DbHelper(getApplicationContext());
        sessionHelper = new SessionHelper(getApplicationContext());
        dialog = new ProgressDialog(WeatherDetailsActivity.this);
        weatherObject = new WeatherObject();
        connectivityHelper = new ConnectivityHelper();

        weatherObject = dbHelper.getWeatherPerLocation(sessionHelper.getLocationId());

        TextView tvLocation = (TextView)findViewById(R.id.tvLocationDetail);
        TextView tvTemperatureDetail = (TextView)findViewById(R.id.tvTemperatureDetail);
        TextView tvPressureDetail = (TextView)findViewById(R.id.tvPressureDetail);
        TextView tvHumidityDetail = (TextView)findViewById(R.id.tvHumidityDetail);
        TextView tvWeatherDetail = (TextView)findViewById(R.id.tvWeatherDetail);
        ImageView ivIcon = (ImageView)findViewById(R.id.ivIcon);
        Button btnRefresh = (Button)findViewById(R.id.btnRefreshDetails);

        tvLocation.setText(weatherObject.getName());
        tvTemperatureDetail.setText(weatherObject.getTemp() + (char) 0x00B0 + "\n \n Temperature");
        tvPressureDetail.setText(weatherObject.getPressure()  + "\n \n Pressure");
        tvHumidityDetail.setText(weatherObject.getHumidity() + "%"  + "\n \n Humidity");
        tvWeatherDetail.setText(weatherObject.getMain());

        String imageUrl = "http://openweathermap.org/img/w/" + weatherObject.getIcon() + ".png";

        Ion.with(ivIcon)
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .load(imageUrl);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connectivityHelper.isConnected()) {
                    dialog.setMessage("Checking the latest weather update...");
                    dialog.show();
                    getWeather();
                } else {
                    Toast.makeText(WeatherDetailsActivity.this, "Internet Connection required!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void getWeather() {

        StringRequest getRequest = new StringRequest(Request.Method.GET, "https://api.openweathermap.org/data/2.5/group?id=5391959,4548393,2643743&units=metric&appid=d51b6a7471f328c8373597e85d6c2dd0", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dbHelper.addWeather(response);
                sessionHelper.setDateToday();
                dialog.dismiss();
                if(sessionHelper.getTag()=="1"){
                    sessionHelper.setTag("0");
                    startActivity(new Intent(getApplicationContext(),WeatherDetailsActivity.class));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),  error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("error",error.getMessage());
            }
        });
        getRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(getRequest);
    }
}
