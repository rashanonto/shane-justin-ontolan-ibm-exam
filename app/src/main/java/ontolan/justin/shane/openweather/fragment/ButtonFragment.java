package ontolan.justin.shane.openweather.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import ontolan.justin.shane.openweather.Activity.CurrentLocationActivity;
import ontolan.justin.shane.openweather.Activity.MainActivity;
import ontolan.justin.shane.openweather.R;
import ontolan.justin.shane.openweather.helper.ConnectivityHelper;


public class ButtonFragment extends Fragment {
    private ConnectivityHelper connectivityHelper;
    // This method will be invoked when the Fragment view object is created.
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View retView = inflater.inflate(R.layout.fragment_button, container);
        connectivityHelper = new ConnectivityHelper();
        if(retView!=null) {
            ListView lvWeatherLsit = (ListView) retView.findViewById(R.id.lvWeather);
            Button btnRefresh = (Button)retView.findViewById(R.id.btnRefresh);
            Button btnCurrentLocation = (Button)retView.findViewById(R.id.btnMyLocation);
            btnRefresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(connectivityHelper.isConnected()) {
                        startActivity(new Intent(getActivity(), MainActivity.class));
                    } else Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();

                }
            });

            btnCurrentLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   statusCheck();
                }
            });
        }
        return retView;
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}