package ontolan.justin.shane.openweather.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import ontolan.justin.shane.openweather.R;
import ontolan.justin.shane.openweather.object.WeatherObject;

public class MainWeatherListAdapter extends BaseAdapter {

    private List<WeatherObject> listData;
    private LayoutInflater layoutInflater;

    public MainWeatherListAdapter(Context context, List<WeatherObject> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);

    }
    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.lv_main, null);
//            if (position % 2 == 1) {
//                holder.tvLocation = (TextView)convertView.findViewById(R.id.tvLocation);
//               // holder.tvLocation.setTextColor(Color.parseColor("#221d1d"));
//            } else {
//                holder.tvLocation = (TextView)convertView.findViewById(R.id.tvLocation);
//                //holder.tvLocation.setTextColor(Color.parseColor("#eade98"));
//                //holder.tvLocation.setTextColor(Color.parseColor("#eade98"));
//                // holder.tvBranchName.setTextColor(Color.parseColor("#ffffff"));
//            }
            holder.tvLocation = (TextView)convertView.findViewById(R.id.tvLocation);
            holder.tvWeather = (TextView) convertView.findViewById(R.id.tvWeather);
            holder.tvTemperature = (TextView) convertView.findViewById(R.id.tvTemperature);
            holder.ivMainIcon = (ImageView) convertView.findViewById(R.id.ivMainIcon);
            holder.tvLocationId = (TextView) convertView.findViewById(R.id.tvLocationId);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvLocation.setText(listData.get(position).getName().toString());
        holder.tvWeather.setText("weather : " + listData.get(position).getDescription().toString());
        holder.tvTemperature.setText("temperature : " + listData.get(position).getTemp().toString() + (char) 0x00B0);
        holder.tvLocationId.setText(listData.get(position).getId().toString());

        String imageUrl = "http://openweathermap.org/img/w/" + listData.get(position).getIcon().toString() + ".png";

        Ion.with(holder.ivMainIcon)
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .load(imageUrl);
        return convertView;
    }

    static class ViewHolder {
        TextView tvLocation;
        TextView tvWeather;
        TextView tvTemperature;
        TextView tvLocationId;
        ImageView ivMainIcon;
    }
}