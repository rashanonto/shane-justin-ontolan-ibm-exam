package ontolan.justin.shane.openweather.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import ontolan.justin.shane.openweather.object.WeatherObject;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "db_openWeather";

    public static final String DB_TABLE_WEATHERLIST = "tblweatherlist";


    public static final String DB_KEY_WEATHER_LONGITUDE = "lon";
    public static final String DB_KEY_WEATHER_LATITUDE = "lat";

    public static final String DB_KEY_WEATHER_MAIN = "main";
    public static final String DB_KEY_WEATHER_DESCRIPTION = "description";
    public static final String DB_KEY_WEATHER_ICON = "icon";

    public static final String DB_KEY_WEATHER_TEMPERATURE = "temp";
    public static final String DB_KEY_WEATHER_PRESSURE = "pressure";
    public static final String DB_KEY_WEATHER_HUMIDITY = "humidity";
    public static final String DB_KEY_WEATHER_TEMPERATURE_MIN = "temp_min";
    public static final String DB_KEY_WEATHER_TEMPERATURE_MAX = "temp_max";
    public static final String DB_KEY_WEATHER_SEA_LEVEL = "sea_level";
    public static final String DB_KEY_WEATHER_GROUND_LEVEL = "grnd_level";

    public static final String DB_KEY_WEATHER_WIND_SPEED = "speed";

    public static final String DB_KEY_WEATHER_LOCATION = "name";
    public static final String DB_KEY_WEATHER_ID = "id";

    private Context mContext;
    private String API_URL = "";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.mContext = context;

        API_URL = mContext.getSharedPreferences("API_URL", Context.MODE_PRIVATE).getString("API_URL", "");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("pasok","pasok");
        String createTableWeather = "CREATE TABLE " + DB_TABLE_WEATHERLIST +
                "(location_id INTEGER PRIMARY KEY, " +
                DB_KEY_WEATHER_LONGITUDE + " TEXT, " +
                DB_KEY_WEATHER_LATITUDE + " TEXT, " +
                DB_KEY_WEATHER_MAIN + " TEXT, " +
                DB_KEY_WEATHER_DESCRIPTION + " TEXT, " +
                DB_KEY_WEATHER_ICON + " TEXT, " +
                DB_KEY_WEATHER_TEMPERATURE + " TEXT, " +
                DB_KEY_WEATHER_PRESSURE + " TEXT, " +
                DB_KEY_WEATHER_HUMIDITY + " TEXT, " +
                DB_KEY_WEATHER_TEMPERATURE_MIN + " TEXT, " +
                DB_KEY_WEATHER_TEMPERATURE_MAX + " TEXT, " +
                DB_KEY_WEATHER_GROUND_LEVEL + " TEXT, " +
                DB_KEY_WEATHER_WIND_SPEED + " TEXT, " +
                DB_KEY_WEATHER_SEA_LEVEL + " TEXT, " +
                DB_KEY_WEATHER_LOCATION + " TEXT, " +
                DB_KEY_WEATHER_ID + " TEXT )";

        db.execSQL(createTableWeather);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_WEATHERLIST);

        onCreate(db);
    }

    public void addWeather(String jsonString){
        Cursor c = this.getReadableDatabase().rawQuery("SELECT * FROM " + DB_TABLE_WEATHERLIST, null);
        if (c.getCount() > 0) this.getWritableDatabase().delete(DB_TABLE_WEATHERLIST, null, null);
        c.close();
        this.close();
        try {
            JSONObject object = new JSONObject(jsonString);
            JSONArray jsonArray = object.getJSONArray("list");
            Log.d("list",jsonArray.toString());
            for(int n = 0; n < jsonArray.length(); n++) {

                JSONObject objects = jsonArray.getJSONObject(n);

                String id = objects.getString("id").toString();
                String name = objects.getString("name").toString();
                JSONObject jsonCoord = new JSONObject(objects.getString("coord"));
                String longitude = jsonCoord.getString("lon");
                String latitude = jsonCoord.getString("lat");

                JSONArray weatherArray = objects.getJSONArray("weather");
                JSONObject weatherJson = weatherArray.getJSONObject(0);

                String main = weatherJson.getString("main");
                String description = weatherJson.getString("description");
                String icon = weatherJson.getString("icon");

                JSONObject mainJson = new JSONObject(objects.getString("main"));

                String temp = mainJson.getString("temp");
                String pressure = (mainJson.has("pressure") ? mainJson.getString("pressure") : "-" );
                String humidity = (mainJson.has("humidity") ? mainJson.getString("humidity") : "-" );
                String tempMin = (mainJson.has("temp_min") ? mainJson.getString("temp_min") : "-" );
                String tempMax = (mainJson.has("temp_max") ? mainJson.getString("temp_max") : "-" );
                String seaLevel = (mainJson.has("sea_level") ? mainJson.getString("sea_level") : "-" );
                String grndLevel = (mainJson.has("grnd_level") ? mainJson.getString("grnd_level") : "-" );

                JSONObject speedJson = new JSONObject(objects.getString("wind"));

                String windSpeed = (speedJson.has("speed") ? speedJson.getString("speed") : "-");

                ContentValues values = new ContentValues();

                values.put(DB_KEY_WEATHER_LONGITUDE, longitude);
                values.put(DB_KEY_WEATHER_LATITUDE, latitude);
                values.put(DB_KEY_WEATHER_MAIN, main);
                values.put(DB_KEY_WEATHER_DESCRIPTION, description);
                values.put(DB_KEY_WEATHER_ICON, icon);
                values.put(DB_KEY_WEATHER_TEMPERATURE, temp);
                values.put(DB_KEY_WEATHER_PRESSURE, pressure);
                values.put(DB_KEY_WEATHER_HUMIDITY, humidity);
                values.put(DB_KEY_WEATHER_TEMPERATURE_MIN, tempMin);
                values.put(DB_KEY_WEATHER_TEMPERATURE_MAX, tempMax);
                values.put(DB_KEY_WEATHER_GROUND_LEVEL, grndLevel);
                values.put(DB_KEY_WEATHER_WIND_SPEED, windSpeed);
                values.put(DB_KEY_WEATHER_SEA_LEVEL, seaLevel);
                values.put(DB_KEY_WEATHER_LOCATION, name);
                values.put(DB_KEY_WEATHER_ID, id);
                Log.d("detalye",id + "," +name);
                this.getWritableDatabase().insert(DB_TABLE_WEATHERLIST, null, values);
                this.close();
             }

        } catch (JSONException e){
            Log.d("errorJson",e.toString());
        }
    }

    public List<WeatherObject> getWeatherList() {
        List<WeatherObject> weatherObjects = new ArrayList<>();

        Cursor c = this.getReadableDatabase().rawQuery("SELECT * FROM " + DB_TABLE_WEATHERLIST, null);
            Log.d("getCOunt",String.valueOf(c.getCount()));
        if (c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                WeatherObject object = new WeatherObject();
                object.setLon(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_LONGITUDE)));
                object.setLat(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_LATITUDE)));
                object.setMain(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_MAIN)));
                object.setDescription(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_DESCRIPTION)));
                object.setIcon(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_ICON)));
                object.setTemp(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_TEMPERATURE)));
                object.setPressure(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_PRESSURE)));
                object.setHumidity(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_HUMIDITY)));
                object.setTemp_min(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_TEMPERATURE_MIN)));
                object.setTemp_max(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_TEMPERATURE_MAX)));
                object.setGrnd_level(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_GROUND_LEVEL)));
                object.setSpeed(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_WIND_SPEED)));
                object.setSea_level(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_SEA_LEVEL)));
                object.setName(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_LOCATION)));
                object.setId(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_ID)));
                weatherObjects.add(object);
                c.moveToNext();
            }
        }
        c.close();
        this.close();
        return weatherObjects;
    }

    public WeatherObject getWeatherPerLocation(String locationId) {
        WeatherObject object = new WeatherObject();

        Cursor c = this.getReadableDatabase().rawQuery("SELECT * FROM " + DB_TABLE_WEATHERLIST + " WHERE " + DB_KEY_WEATHER_ID + " = " + locationId, null);
        Log.d("getCOunt",String.valueOf(c.getCount()));
        if (c.getCount() > 0) {
            c.moveToFirst();
                object.setLon(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_LONGITUDE)));
                object.setLat(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_LATITUDE)));
                object.setMain(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_MAIN)));
                object.setDescription(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_DESCRIPTION)));
                object.setIcon(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_ICON)));
                object.setTemp(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_TEMPERATURE)));
                object.setPressure(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_PRESSURE)));
                object.setHumidity(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_HUMIDITY)));
                object.setTemp_min(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_TEMPERATURE_MIN)));
                object.setTemp_max(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_TEMPERATURE_MAX)));
                object.setGrnd_level(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_GROUND_LEVEL)));
                object.setSpeed(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_WIND_SPEED)));
                object.setSea_level(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_SEA_LEVEL)));
                object.setName(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_LOCATION)));
                object.setId(c.getString(c.getColumnIndexOrThrow(DB_KEY_WEATHER_ID)));
            }

        c.close();
        this.close();
        return object;
    }
}


