package ontolan.justin.shane.openweather.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ontolan.justin.shane.openweather.Activity.MainActivity;
import ontolan.justin.shane.openweather.Activity.WeatherDetailsActivity;
import ontolan.justin.shane.openweather.R;
import ontolan.justin.shane.openweather.adapter.MainWeatherListAdapter;
import ontolan.justin.shane.openweather.helper.DbHelper;
import ontolan.justin.shane.openweather.helper.SessionHelper;
import ontolan.justin.shane.openweather.object.WeatherObject;


public class ListFragment extends Fragment {

    // This method will be invoked when the Fragment view object is created.
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       final View retView = inflater.inflate(R.layout.fragment_main, container);

        // Get Fragment belonged Activity
        final FragmentActivity fragmentBelongActivity = getActivity();

        if(retView!=null) {
            ListView lvWeatherLsit = (ListView) retView.findViewById(R.id.lvWeather);
            final SessionHelper sessionHelper = new SessionHelper(getActivity());
            List<WeatherObject> weatherObjects;
            DbHelper dbHelper = new DbHelper(getActivity());
            weatherObjects = dbHelper.getWeatherList();

            MainWeatherListAdapter adapter = new MainWeatherListAdapter(getActivity(), weatherObjects);
            lvWeatherLsit.setAdapter(adapter);

            lvWeatherLsit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String locationId = ((TextView) view.findViewById(R.id.tvLocationId)).getText().toString();
                    Log.d("locationId", locationId);
                    sessionHelper.setLocationId(locationId);
                    startActivity(new Intent(getActivity(), WeatherDetailsActivity.class));
                }
            });
        }
        return retView;
    }
}