package ontolan.justin.shane.openweather.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import ontolan.justin.shane.openweather.R;
import ontolan.justin.shane.openweather.helper.ConnectivityHelper;
import ontolan.justin.shane.openweather.helper.DbHelper;
import ontolan.justin.shane.openweather.helper.SessionHelper;

import static ontolan.justin.shane.openweather.Activity.MainActivity.MY_PERMISSIONS_REQUEST_LOCATION;

/**
 * Created by rashanonto on 2018-06-14.
 */

public class SplashActivity extends AppCompatActivity {

    private DbHelper dbHelper;
    private SessionHelper sessionHelper;
    private ConnectivityHelper connectivityHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_testing);

        dbHelper = new DbHelper(getApplicationContext());
        sessionHelper = new SessionHelper(getApplicationContext());



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sessionHelper.setTag("1");
//                if(connectivityHelper.isConnected()) {
//                    getWeather();
//                }
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            }
        }, 2000);

    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Request Permission..")
                        .setMessage("Requesting Permission to open location..")
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(SplashActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    public void getWeather() {

        StringRequest getRequest = new StringRequest(Request.Method.GET, "https://api.openweathermap.org/data/2.5/group?id=5391959,4548393,2643743&units=metric&appid=d51b6a7471f328c8373597e85d6c2dd0", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dbHelper.addWeather(response);
                sessionHelper.setDateToday();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),  error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("error",error.getMessage());
            }
        });
        getRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(getRequest);
    }
}
